/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practiceThreeQA;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ramcev
 */
public class mainPracticeTest {
    
    public mainPracticeTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of reviewString method, of class mainPractice.
     */
    @Test
    public void testReviewString() {
        System.out.println("reviewString");
        String cadena = "-2*!#œ-";
        String expResult = "Result:---> 2*!#œ";
        String result = mainPractice.reviewString(cadena);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        if (cadena.equals(expResult)) {
            fail("The test case is a prototype.");
        }
        
    }
    
}
